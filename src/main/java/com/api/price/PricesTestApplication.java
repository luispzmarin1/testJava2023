package com.api.price;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PricesTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(com.api.price.PricesTestApplication.class, args);
	}

}
