package com.api.price.repository;

import com.api.price.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PriceRepository extends JpaRepository <Price, Long> {


    /** Query que devuelve los datos filtrados y ordenados por prioridad **/
    @Query("SELECT p FROM Price p " +
            "JOIN p.product pr " +
            "WHERE pr.id = :productId " +
            "AND (:date BETWEEN p.startDate AND p.endDate)" +
            "ORDER BY p.priority DESC")
    List<Price> findByProductAndDates(
            LocalDateTime date,
            Long productId);

}
