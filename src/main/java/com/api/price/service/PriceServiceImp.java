package com.api.price.service;

import com.api.price.model.Price;
import com.api.price.repository.PriceRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;


@Service
@AllArgsConstructor
public class PriceServiceImp implements PriceService{

    private final PriceRepository priceRepository;

    @Override
    public List<Price> findRate(LocalDateTime date, Long productId){
        return priceRepository.findByProductAndDates(date, productId);
    }

}
