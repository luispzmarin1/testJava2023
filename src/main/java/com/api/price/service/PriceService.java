package com.api.price.service;

import com.api.price.model.Price;
import java.time.LocalDateTime;
import java.util.List;

public interface PriceService {

    /** Servicio usado en el ejercicio**/
    List<Price> findRate(LocalDateTime date, Long idProduct);

}
