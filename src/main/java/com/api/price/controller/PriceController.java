package com.api.price.controller;

import com.api.price.controller.request.RateRequest;
import com.api.price.controller.response.ErrorResponse;
import com.api.price.controller.response.RateResponse;
import com.api.price.model.Price;
import com.api.price.service.PriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/prices")
@RequiredArgsConstructor
public class PriceController {

    private final PriceService priceService;

    @PostMapping("/rate")
    public ResponseEntity findRate(@RequestBody RateRequest json) throws Exception {
        try {
            //Se parsea la fecha con el formato del enunciado
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss");
            LocalDateTime timestamp = LocalDateTime.parse(json.getDate(), formatter);
            List<Price> Lprice= priceService.findRate(timestamp , json.getProductId());

            //Se revisa si la consulta no trajo resultados
            if(Lprice.isEmpty()){
                return new ResponseEntity(new ErrorResponse(HttpStatus.NOT_FOUND),HttpStatus.NOT_FOUND);
            }else{
                Price rPrice = Lprice.get(0);
                return new ResponseEntity( new RateResponse(rPrice.getPrice(), rPrice.getProduct().getId(),
                        rPrice.getStartDate(), timestamp, rPrice.getEndDate(), rPrice.getPriceList()), HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity(new ErrorResponse(HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        }
    }




}
