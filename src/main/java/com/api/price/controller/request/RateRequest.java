package com.api.price.controller.request;

public class RateRequest {
    private String date;
    private Long productId;

    public RateRequest(String date, Long productId) {
        this.date = date;
        this.productId = productId;
    }

    public String getDate() {
        return this.date;
    }

    public Long getProductId() {
        return this.productId;
    }

}

