package com.api.price.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table

public class Price {

    @Column(nullable = false)
    private LocalDateTime startDate;

    @Column(nullable = false)
    private LocalDateTime endDate;

    @Column(nullable = false)
    private int priceList;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;


    private int priority;

    @Column(nullable = false)
    private Double price;
    @Enumerated(EnumType.STRING)
    private Currency curr;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    public Price(LocalDateTime startDate, LocalDateTime endDate, int priceList, Product product, int priority, Double price, Currency curr, Long id) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceList = priceList;
        this.product = product;
        this.priority = priority;
        this.price = price;
        this.curr = curr;
        this.id = id;
    }

    public Price() {
    }

    public LocalDateTime getStartDate() {
        return this.startDate;
    }

    public LocalDateTime getEndDate() {
        return this.endDate;
    }

    public int getPriceList() {
        return this.priceList;
    }

    public Product getProduct() {
        return this.product;
    }

    public int getPriority() {
        return this.priority;
    }

    public Double getPrice() {
        return this.price;
    }

    public Currency getCurr() {
        return this.curr;
    }

    public Long getId() {
        return this.id;
    }
}
