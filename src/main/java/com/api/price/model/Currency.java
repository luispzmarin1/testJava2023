package com.api.price.model;

public enum Currency {
    EUR("Euro"),
    DOL("Dolar"),
    UNK("Unknown");
    private final String curr;

    Currency(String curr) {
        this.curr = curr;
    }


}
