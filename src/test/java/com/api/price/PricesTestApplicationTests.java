package com.api.price;

import com.api.price.controller.PriceController;
import com.api.price.controller.request.RateRequest;
import com.api.price.controller.response.RateResponse;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest
class PricesTestApplicationTests {

	@Autowired
	private PriceController priceController;

	@ParameterizedTest
	@CsvFileSource(resources = "/testData.csv", numLinesToSkip = 1)
	void restIntegrationTest(ArgumentsAccessor argumentsAccessor) throws Exception {
		//Se crea el request con los datos del CSV
		RateRequest rateRequest = new RateRequest(argumentsAccessor.getString(1),
				argumentsAccessor.getLong(2));
		//Se simula la llamada
		ResponseEntity <RateResponse> rateResponse = priceController.findRate(rateRequest);
		//Se revisa si la llamada ha sido la esperada
		if (rateResponse.getStatusCode() == HttpStatus.OK) {
			RateResponse price = rateResponse.getBody();
			if (price != null) {
				//Se revisa si el resultado devuelto es el esperado
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss");
				System.out.println(price.getPrice());
				assertEquals(price.getProductId(),
						argumentsAccessor.getLong(2)); // PRODUCT_ID
				assertEquals(price.getPriceList(),
						argumentsAccessor.getLong(3)); // PRICE_LIS_RES
				assertEquals(price.getStartDateRate(),
						LocalDateTime.parse(argumentsAccessor.getString(4), formatter)); // START_DATE_RES
				assertEquals(price.getEndDateRate(),
						LocalDateTime.parse(argumentsAccessor.getString(5), formatter)); // END_DATE_RES
				assertEquals(price.getPrice(),
						argumentsAccessor.getDouble(6)); // PRICE_RES
			}
		} else {
			// Se revisa si el test esperaba devolver NOT_FOUND
			if (argumentsAccessor.getBoolean(0)){
				System.err.println(rateResponse.getStatusCode());
				fail("El test debería haber encontrado la tarifa");
			}
		}
	}

}
