En la base de datos de la compañía disponemos de la tabla PRICES que refleja el precio final (pvp) y la tarifa que aplica a un producto. 
A continuación se muestra un ejemplo de la tabla con los campos relevantes:
 
PRICES
-------
 
START_DATE                END_DATE                    PRICE_LIST     PRODUCT_ID    PRIORITY     PRICE           CURR
---------------------------------------------------------------------------------------------------------------------
2020-06-14-00.00.00       2020-12-31-23.59.59         1              35455         0            35.50           EUR
2020-06-14-15.00.00       2020-06-14-18.30.00         2              35455         1            25.45           EUR
2020-06-15-00.00.00       2020-06-15-11.00.00         3              35455         1            30.50           EUR
2020-06-15-16.00.00       2020-12-31-23.59.59         4              35455         1            38.95           EUR
 
Campos: 
 
START_DATE , END_DATE: Rango de fechas en el que aplica el precio tarifa indicado.
PRICE_LIST: Identificador de la tarifa de precios aplicable.
PRODUCT_ID: Identificador código de producto.
PRIORITY: Desambiguador de aplicación de precios. Si dos tarifas coinciden en un rango de fechas se aplica la de mayor prioridad (mayor valor numérico).
PRICE: Precio final de venta.
CURR: Moneda.
 
Se pide:
 
Construir una aplicación/servicio en SpringBoot (si no se conoces SpringBoot no es necesario utilizarlo) que provea un endpoint rest de consulta  tal que:
 
Acepte como parámetros de entrada: fecha de aplicación, identificador de producto.
Devuelva como datos de salida: identificador de producto, tarifa a aplicar, fechas de aplicación y precio final a aplicar.
 
Lo ideal es  utilizar una base de datos en memoria (tipo h2) e inicializar con los datos del ejemplo. También se puede utilizar otra BD si no se esta familiarizado con BD en memoria.

(se pueden cambiar el nombre de los campos y añadir otros nuevos si se quiere, elegir el tipo de dato que se considere adecuado para los mismos).
              
Desarrollar unos test al endpoint rest que  validen las siguientes peticiones al servicio con los datos del ejemplo:
                                                                                       
-          Test 1: petición a las 10:00 del día 14 del producto 35455   
-          Test 2: petición a las 16:00 del día 14 del producto 35455   
-          Test 3: petición a las 21:00 del día 14 del producto 35455   
-          Test 4: petición a las 10:00 del día 15 del producto 35455   
-          Test 5: petición a las 21:00 del día 16 del producto 35455   
 
 
Condiciones 
 
Se valorará:
 
 1) Diseño y construcción del servicio.
 2) Calidad de Código.
 3) Resultados correctos en los test.
 4) Si no se llega al resultado final, se valorará todo aquello que se haya desarrollado con respecto a las tres premisas anteriores


Gracias
