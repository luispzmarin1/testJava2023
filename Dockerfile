FROM openjdk:18
# Custom cache invalidation
ARG CACHEBUST=1
ADD target/price-0.0.1.jar /developments/
ENTRYPOINT ["java","-jar","/developments/price-0.0.1.jar"]